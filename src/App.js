import 'bootstrap/dist/css/bootstrap.min.css'
import React from 'react';
import DefaultLayout from "./view/layout-configurator/default";
import ProtectedRoute from './component/route/protected';
import GuestRoute from './component/route/guest';
import Login from './component/login/container';

function App() {
  return (
      <React.Fragment>
        <GuestRoute path='/login' component={ Login } />
        <ProtectedRoute component={ DefaultLayout } />
      </React.Fragment>
  )
}

export default App;
