const CREATE_API = 'api:new';
const DELETE_API = 'api:delete';
const FETCH_API = 'api:fetch';
const FETCHED_API = 'api:fetched';
const UPDATE_API = 'api:update';

export { CREATE_API, DELETE_API, FETCH_API, FETCHED_API, UPDATE_API }