import endpoint from './../api/invitation'

const add = (guests, team) => dispatch => {
    return endpoint.add(guests, team)
}

export { add }