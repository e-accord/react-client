import * as type from './api-types';
import endpoint from './../api/api';

const createApi = api => {
    return {
        type: type.CREATE_API,
        payload: api
    }
}

const apiFetched = apis => {
    return {
        type: type.FETCHED_API,
        payload: apis
    }
}

const deleteApi = id => {
    return {
        type: type.DELETE_API,
        payload: id
    }
}

const updateApi = payload => {
    return {
        type: type.UPDATE_API,
        payload
    }
}

const fetchOne = id => dispatch => {
    return endpoint(dispatch).get(id);
}

const fetch = () => dispatch => {
    return endpoint(dispatch).list().then(apis => dispatch(apiFetched(apis)) )
}

const add = ({name, description = ''}) => dispatch => {
    return endpoint(dispatch).add(name, description).then(api => dispatch(createApi(api)));
}

const remove = id => dispatch => {
    return endpoint(dispatch).remove(id).then(() => dispatch(deleteApi(id)));
}

const update = payload => dispatch => {
    return endpoint(dispatch).update(payload).then(api => dispatch(updateApi(api)))
}

export { fetchOne, remove, fetch, add, update }