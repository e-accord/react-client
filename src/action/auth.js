import * as type from './auth-types';
import endpoint from './../api/auth';
import setAuthHeader from './../utils/auth-header';

const userLogedIn = user => {
    return {
        type: type.AUTH_LOGEDIN,
        payload: user
    }
}

const userLogedOut = () => {
    localStorage.removeItem('apiCreatorToken')
    return {
        type: type.AUTH_LOGEDOUT
    }
}

const login = payload => dispatch => {
    return endpoint.login(payload).then(user => {
        localStorage.setItem('apiCreatorToken', user.token)
        setAuthHeader(user.token);
        dispatch(userLogedIn(user))
    } );
}

const logout = () => dispatch => {
    setAuthHeader(null);
    return dispatch(userLogedOut())
}

export { login, logout, userLogedIn, userLogedOut }