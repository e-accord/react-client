const AUTH_LOGIN = 'auth:login'
const AUTH_LOGEDIN = 'auth:logedin'
const AUTH_LOGEDOUT = 'auth:logedout'

export { AUTH_LOGIN, AUTH_LOGEDIN, AUTH_LOGEDOUT };