import * as type from './team-types'
import endpoint from './../api/team';

const createdTeam = team => {
    return {
        type: type.CREATE_TEAM,
        payload: team
    }
}

const add = name => dispatch => {
    return endpoint.add(name)
        .then(api => {
            dispatch(createdTeam(api));
            return api;
        })
}

export { add }