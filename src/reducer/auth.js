import * as type from './../action/auth-types';

const authInitialState = {
    token: null
}

const authReducer = (state = authInitialState, action) => {
    switch (action.type) {
        case type.AUTH_LOGEDIN:
            return {...action.payload}
        case type.AUTH_LOGEDOUT:
            return {token: null}
        default:
            return state
    }
}

export { authReducer, authInitialState };