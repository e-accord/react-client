import * as type from './../action/api-types';

const apiInitialState = {
    list: []
}

const apiReducer = (state = apiInitialState, action) => {
    switch (action.type) {
        case type.CREATE_API:
            return { ...state, list: [...state.list, action.payload] }
        case type.FETCH_API:
            return { ...state.list.filter( api => api._id === action.payload ) };
        case type.DELETE_API:
            return { ...state, list: [...state.list.filter(api => api._id !== action.payload)] }
        case type.FETCHED_API:
            return {list: [...action.payload]}
        case type.UPDATE_API:
            return {...state, list: [...state.list.map(api => {
                if(action.payload._id === api._id){
                    return action.payload
                }

                return api;
            })]}
        default:
            return state
    }
}

export { apiReducer, apiInitialState };