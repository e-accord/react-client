import { composeWithDevTools } from "redux-devtools-extension";
import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { apiReducer, apiInitialState } from './reducer/api';
import { authReducer, authInitialState } from './reducer/auth';

const configureStore = () => {

    const reducers = combineReducers({
        api: apiReducer,
        auth: authReducer
    });

    return createStore(
        reducers,
        {
            api: apiInitialState,
            auth: authInitialState
        },
        composeWithDevTools(
            applyMiddleware(thunk)
        )
    )
}

export default configureStore;