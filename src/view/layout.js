import React from 'react';
import {PropTypes} from 'prop-types';
import Grid from '@material-ui/core/Grid';

const layout = props => {
    return (
        <React.Fragment>
            <Grid container component='header'>
                {props.children('header')}
            </Grid>
            <Grid container component='main'>
                {props.children('main')}
            </Grid >
            <Grid container component='footer'>
                {props.children('footer')}
                <small>&copy; 2019 @ekanto</small>
            </Grid>
        </React.Fragment>
    );
}

const withHeader = HeaderComponent => Layout => {
    const WithHeader =  props => (
        <Layout {...props} >
            {part => part === 'header' ? <HeaderComponent {...props} /> : props.children && props.children(part)}
        </Layout>
    )

    WithHeader.propTypes = {
        children: PropTypes.func
    }

    WithHeader.defaultProps = {
        children: null
    }

    return WithHeader;
}

const withMain = MainComponent => Layout => {
    const WithMain = props => (
        <Layout {...props}>
            {part => part === 'main' ? <MainComponent {...props} /> : props.children && props.children(part)}
        </Layout>
    )

    WithMain.propTypes = {
        children: PropTypes.func
    }

    WithMain.defaultProps = {
        children: null
    }

    return WithMain;
}

const withFooter = FooterComponent => Layout => {
    const WithFooter = props => (
        <Layout {...props}>
            {part => part === 'footer' ? <FooterComponent {...props} /> : props.children && props.children(part)}
        </Layout>
    )

    WithFooter.propTypes = {
        children: PropTypes.func
    }

    WithFooter.defaultProps = {
        children: null
    }

    return WithFooter;
}

layout.propTypes = {
    children: PropTypes.func.isRequired
}

export { layout, withHeader, withMain, withFooter}