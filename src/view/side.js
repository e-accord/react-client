import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import { makeStyles } from '@material-ui/core/styles';

import ApiSideContainer from './../component/api/side-container';

const drawerWidth = 'inherit';

const useStyles = makeStyles(theme => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
}));


const Side = () => {
    const classes = useStyles();
    return (
        <div style={{display: 'flex'}}>
            <Drawer
                anchor="left"
                className={classes.drawer}
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.toolbar} />
                <List>
                    <ApiSideContainer />
                </List>
            </Drawer>
        </div>
    );
}

export default Side;