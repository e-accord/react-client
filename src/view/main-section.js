import React from 'react';
import { Switch } from 'react-router-dom'
import uuid from "uuid";
import { makeStyles } from '@material-ui/core/styles';
import navs from './../conf/nav';
import Spinner from './../utils/spinner'
import ProtectedRoute from './../component/route/protected';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
    toolbar: theme.mixins.toolbar,
    root: {
        padding: theme.spacing(3, 2),
        marginRight: '5px'
    },
}));

const MainSection = () => {
    const classes = useStyles();
    return (
        <React.Fragment>
            <div className={classes.toolbar} />
            <Paper className={classes.root} >
                <Switch>
                    <React.Suspense fallback={Spinner}>
                        {navs.map(nav => <ProtectedRoute key={uuid.v1()} exact={nav.exact} path={nav.path} component={nav.component} />)}
                    </React.Suspense>
                </Switch>
            </Paper>
        </React.Fragment>
    )
}

export default MainSection;