import { withHeader, layout as Layout, withMain } from './../layout';
import { Main, withSide, withMainSection } from "./../main";
import Header from './../header'
import Side from './../side'
import withAlertProvider from "./../alert-provider";
import MainSection from "./../main-section";

const MainWithSide = withSide(Side)(Main);
const MainWithMainSection = withMainSection(MainSection)(MainWithSide)
const LayoutWithHeader = withHeader(Header)(Layout);
const DefaultLayout = withMain(MainWithMainSection)(LayoutWithHeader);

const MainLayout = withAlertProvider(DefaultLayout);

export default MainLayout