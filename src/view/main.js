import React from 'react';
import { PropTypes } from 'prop-types';
import Grid from '@material-ui/core/Grid';

const Main = props => {
    return (
        <React.Fragment>
            <Grid item sm={2} component='aside'>
                {props.children('side')}
            </Grid>
            <Grid item sm={10} xs={3} component='section'>
                {props.children('main-section')}
            </Grid>
        </React.Fragment>
    );
}

const withSide = SideComponent => MainComponent => {
    const WithSide =  props => (
        <MainComponent {...props}>
            {part => part === 'side' ? <SideComponent {...props} /> : props.children && props.children(part)}
        </MainComponent>
    )

    WithSide.propTypes = {
        children: PropTypes.func
    }

    WithSide.defaultProps = {
        children: null
    }

    return WithSide;
}

const withMainSection = MainSectionComponent => MainComponent => {
    const WithMainSection = props => (
        <MainComponent {...props}>
            {part => part === 'main-section' ? <MainSectionComponent {...props} /> : props.children && props.children(part)}
        </MainComponent>
    )

    WithMainSection.propTypes = {
        children: PropTypes.func
    }

    WithMainSection.defaultProps = {
        children: null
    }

    return WithMainSection;
}

Main.propTypes = {
    children: PropTypes.func.isRequired
}

export {Main, withSide, withMainSection};