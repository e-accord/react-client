import React from 'react'
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'

const options = {
    position: positions.BOTTOM_RIGHT,
    timeout: 5000,
    offset: '30px',
    transition: transitions.SCALE
}

const withAlertProvider = Component => {
    return () => {
        return (
            <AlertProvider template={AlertTemplate} {...options}>
                <Component />
            </AlertProvider>
        )
    }
}

export default withAlertProvider;