import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { BrowserRouter, Route } from 'react-router-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import configureStore from './configure-store';
import { userLogedIn} from './action/auth';
import setAuthHeader from './utils/auth-header';

const store = configureStore()

if (localStorage.apiCreatorToken) {
    store.dispatch(userLogedIn({token: localStorage.getItem('apiCreatorToken')}));
    setAuthHeader(localStorage.getItem('apiCreatorToken'));
}

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <Route component={App} />
        </Provider>
    </BrowserRouter>,
    document.getElementById('root')
);
serviceWorker.unregister();
