import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import ProfileMenu from './profile-menu';
import { logout } from './../../action/auth';

class ProfileMenuContainer extends React.Component {

    handleLogout = () => {
        this.props.logout();
    }

    render() { 
        return (
            <ProfileMenu onLogout={this.handleLogout} />
        );
    }
}

ProfileMenuContainer.propTypes = {
    logout: PropTypes.func.isRequired
}

export default connect(null, {logout})(ProfileMenuContainer);