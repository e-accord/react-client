import React from 'react';
import { InputGroup, Button, Form, Spinner } from "react-bootstrap";
import Typography from '@material-ui/core/Typography';

const MainForm = ({onSubmit, onChange, loading}) => {
    return (
        <React.Fragment>
            <Typography variant="h5" component="h3" align="center">
                Create new team
            </Typography>
            <div style={{ paddingRight: '180px', paddingLeft: '180px', marginTop: '20px' }}>
                <Form noValidate style={{ display: 'block', width: '100%' }} onSubmit={onSubmit} >
                    <InputGroup className="mb-3">
                        <Form.Control
                            type='text'
                            placeholder="Add new Team"
                            aria-label="Add new Team"
                            aria-describedby="basic-addon2"
                            required
                            onChange={onChange}
                            name='name'
                        />
                        <InputGroup.Append>
                            <Button type='submit' variant="outline-success" >
                                {loading &&
                                    <Spinner
                                        as="span"
                                        animation="border"
                                        size="sm"
                                        role="status"
                                        aria-hidden="true"
                                    />
                                }
                                {!loading && 'Save'}
                            </Button>
                        </InputGroup.Append>
                    </InputGroup>
                </Form>
            </div>
        </React.Fragment>
    )
}

export default MainForm;