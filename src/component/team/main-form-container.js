import React from 'react';
import MainFrom from './main-from'
import { connect } from 'react-redux'
import { add } from './../../action/team'
import InvitationMainContainer from './../invitation/main-container'

class MainFormContainer extends React.Component {
    
    state = { 
        formData: {
            name: ''
        },
        loading: false,
        team: null
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({loading: true})
        this.props.add(this.state.formData)
            .then( team => {
                this.setState({loading: false, team})
            })
            .catch( () => {
                this.setState({loading: false})
            })
    }

    handleChange = e => {
        const {formData} = this.state;
        formData[e.target.name] = e.target.value
        this.setState({
            formData
        })
    }

    render() { 
        return (
            <React.Fragment>
                {
                    !this.state.team && <MainFrom onSubmit={this.handleSubmit} onChange={this.handleChange} loading={this.state.loading} />
                }
                
                {
                    this.state.team && <InvitationMainContainer team={this.state.team} />
                }

            </React.Fragment>
        );
    }
}
 
export default connect(null, { add })(MainFormContainer);