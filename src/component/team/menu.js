import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom'

const TeamMenu = (props) => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    function handleMenu(event) {
        setAnchorEl(event.currentTarget);
    }

    function handleClose() {
        setAnchorEl(null);
    }

    return (
        <div style={{marginRight: '30px'}}>
            <Button style={{color: '#fff'}} aria-controls="simple-menu" aria-haspopup="true" onClick={handleMenu}>
                Team
            </Button>
            <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
            >
                <MenuItem onClick={handleClose}>
                    <Link to='/teams/add-team'>
                        Create team
                    </Link>
                </MenuItem>
                <MenuItem onClick={handleClose}>Invitations</MenuItem>
            </Menu>
        </div>
    );
}

export default TeamMenu;