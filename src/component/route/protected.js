import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

const ProtectedRoute = ({ isAuthenticated, component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props => isAuthenticated ? <Component {...props} /> : <Redirect to="/login" />}
    />
);

ProtectedRoute.propTypes = {
    component: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.objectOf(React.Component)
    ]).isRequired,
    isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
    return {
        isAuthenticated: !!state.auth.token
    };
}

export default connect(mapStateToProps)(ProtectedRoute);
