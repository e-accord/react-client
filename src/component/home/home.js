import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button'
import Spinner from './../../utils/spinner';

const Home = () => {
    return (
        <React.Suspense fallback={Spinner}>
            <div style={{padding: '180px'}}>
                <Jumbotron>
                    <h1>API Creator</h1>
                    <p>
                        We will add couple of extra informations right here as soon as possible !
                    stay tunes
                    </p>
                    <p>
                        <Button variant="primary">Learn more</Button>
                    </p>
                </Jumbotron>
            </div>
        </React.Suspense>
    );
}

export default Home