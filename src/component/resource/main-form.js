import React from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Typography from '@material-ui/core/Typography';


const MainForm = () => {
    return (
        <React.Fragment>
            <Typography variant="h5" component="h3" align="center">
                Create new resource
            </Typography>
            <div style={{paddingRight: '180px', paddingLeft: '180px', marginTop: '20px'}}>
                <Form>
                    <Form.Group controlId="path">
                        <Form.Label>Paht *</Form.Label>
                        <Form.Control type="text" />
                        <Form.Text className="text-muted">
                        the path leading to this resource, must start with /
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="name">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="Short description" />
                    </Form.Group>
                    <Form.Group controlId="desciption">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" rows="3" />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Save
                    </Button>
                </Form>
            </div>
        </React.Fragment>
    )
}

export default MainForm;