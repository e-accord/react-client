import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListIcon from '@material-ui/icons/List';
import ListItem from '@material-ui/core/ListItem';
import React from 'react';
import { PropTypes } from "prop-types";

const useStyles = makeStyles(theme => ({
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));
const SideList = props => {

    const classes = useStyles();

    return (
        <React.Fragment>
            <ListItem className={classes.nested}>
                <ListItemIcon>
                    <ListIcon/>
                </ListItemIcon>
                <ListItemText primary={props.name} />
            </ListItem>
        </React.Fragment>
    );
}

SideList.propTypes = {
    name: PropTypes.string.isRequired
}

export default SideList;