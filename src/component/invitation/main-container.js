import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Button, Form, Spinner } from "react-bootstrap";
import uuid from 'uuid'
import MainForm from './main-form';
import { connect } from 'react-redux';
import { add } from './../../action/invitation'

class MainContainer extends React.Component {
    state = { 
        guests: [
            {name: '', email: '', id: uuid.v1()}
        ]
    }

    addGuest = () => {
        this.setState({ ...this.state, guests: [...this.state.guests, { name: '', email: '', id: uuid.v1() } ] })
    }

    handleRemove = id => {
        this.setState(
                {
                    guests: [...this.state.guests.filter( guest => guest.id != id )]
                }
            )
        }
        
    onSubmit = e => {
        e.preventDefault();
        const { guests } = this.state;
        this.props.add(guests, this.props.team._id)
    }
    
    handleChange = data => {
        this.setState(
            {
                guests: [...this.state.guests.map( guest => {
                    
                    if(guest.id === data.id) {
                        return {...guest, ...data}
                    }

                    return guest
                })]
            }
        )
    }

    render() { 
        return (
            <React.Fragment>
                <Typography variant="h5" component="h3" align="center" style={{marginTop: '10px'}}>
                    Invite people for {this.props.team.name}
                </Typography>
                <div style={{ paddingRight: '180px', paddingLeft: '180px', marginTop: '20px' }}>
                    <Form onSubmit={this.onSubmit}>
                        {this.state.guests.map(guest => <MainForm key={guest.id} guest={guest} onRemove={this.handleRemove} onChange={this.handleChange} /> ) }
                        <Button onClick={this.addGuest} variant="light" style={{marginRight: '25px'}} >
                            Add More
                        </Button>
                        <Button type="submit" variant="success" disabled={this.state.guests.length <= 0} >
                            Send Invitations
                        </Button>
                    </Form>
                </div>
            </React.Fragment>
        );
    }
}
 
export default connect(null, {add})(MainContainer);