import React from 'react';
import { Row, Form, Col, Button} from "react-bootstrap";

const MainForm = props => {

    function onChange(e) {
        const data = {
            [e.target.name]: e.target.value,
            id: props.guest.id
        }
        props.onChange(data);
    }

    return (
        <Row style={{ marginBottom: '10px'}}>
            <Col>
                <Form.Control placeholder="First name"  name='name' onChange={onChange}/>
            </Col>
            <Col>
                <Form.Control placeholder="email" name='email' onChange={onChange} />
            </Col>
            <Col xs >
                <Button variant='danger' onClick={ () => props.onRemove(props.guest.id) }>Remove</Button>
            </Col>
        </Row>
    )
}

export default MainForm