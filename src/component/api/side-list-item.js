import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FolderIcon from '@material-ui/icons/Folder';
import React from 'react';
import { PropTypes } from "prop-types";

const SideList = props => {
    return (
        <React.Fragment>
            <ListItemIcon>
                <FolderIcon />
            </ListItemIcon>
            <ListItemText primary={props.name} />
        </React.Fragment>
    );
}

SideList.propTypes = {
    name: PropTypes.string.isRequired
}

export default SideList;