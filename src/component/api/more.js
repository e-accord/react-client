import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { PropTypes } from "prop-types";
import { Link } from 'react-router-dom'

const ITEM_HEIGHT = 48;

const More = props => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (e) => {
        setAnchorEl(e.currentTarget);
    }

    const handleClose = () => {
        setAnchorEl(null);
    }

    return (
        <div>
            <IconButton
                aria-label="More"
                aria-controls="long-menu"
                aria-haspopup="true"
                onClick={handleClick}
            >
                <MoreVertIcon />
            </IconButton>
            <Menu
                id="long-menu"
                anchorEl={anchorEl}
                keepMounted
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        maxHeight: ITEM_HEIGHT * 4.5,
                        width: 200,
                    },
                }}
            >
                <MenuItem onClick={() => {props.handleEdit(); handleClose()}}>
                    Edit
                </MenuItem>
                <MenuItem onClick={handleClose}>
                    <Link to={`/apis/${props.id}/add-description`}>Add description</Link>
                </MenuItem>
                <MenuItem onClick={handleClose}>
                    <Link to={`/apis/${props.id}/add-resource`}>Add Ressource</Link>
                </MenuItem>
                <MenuItem onClick={ () => props.onDelete(props.id)}>
                    Delete
                </MenuItem>
            </Menu>
        </div>
    );
}

More.propTypes = {
    onDelete: PropTypes.func.isRequired,
    handleEdit: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired
}

export default More;