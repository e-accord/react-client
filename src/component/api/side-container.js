import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import React from 'react';
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { withAlert } from 'react-alert';
import { compose } from "redux";
import SideFrom from './side-form'
import SideListItemContainer from './side-list-item-container';
import { fetch, add, fetchOne, update } from './../../action/api';
import Spinner from './../../utils/spinner';

class ApiSideContainer extends React.Component {

    state = {
        name : '',
        btnSaveName: 'Add',
        loading: true
    }

    componentDidMount(){
        this.props.fetch()
            .then(() => this.setState({loading: false}) )
            .catch((err) => {            
                this.setState({loading: false});
                this.props.alert.error(err)}
            )
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({ loading: true})
        const data = {name: this.state.name, desciption: this.state.desciption, _id: this.state._id};
        if(!!this.state._id){
            return this.updateApi(data);
        }

        return this.createApi(data);
    }

    onChange = (e) => {
        this.setState({name: e.target.value});
    }

    onEdit = id => {
        this.props.fetchOne(id)
            .then(api => this.setState({ ...api, loading: false, btnSaveName: 'Done'}))
            .catch( () => {
                this.setState({loading: false})
                this.props.alert.error('Can not fetch api');
            })
    }

    createApi(data) {
        this.props.add(data)
            .then(() => this.setState({ loading: false, name: '' }))
            .catch(() => {
                this.props.alert.error('Something went wrong durring data saving');
                this.setState({ loading: false });
            });
    }

    updateApi(data) {
        this.props.update(data)
            .then( () => this.setState({loading: false, btnSaveName: 'Add', name: '', _id: null}))
            .catch(() => {
                this.props.alert.error('Something went wrong durring data saving');
                this.setState({ loading: false });
            });
    }
    
    static renderNoApiMessage() {
        return (
            <div style={{ textAlign: 'center', margin: '15px' }}>
                <h3>Let&#39;s start!</h3>
                <p>To bigin, create an api ...</p>
            </div>
        )
    }

    render() { 
        return (
            <React.Fragment>
                <ListItem>
                    <SideFrom loading={this.state.loading} btnSaveName={this.state.btnSaveName} name={this.state.name} {...this.props} handleSubmit={this.onSubmit} handleChange={this.onChange} />
                </ListItem>
                <Divider />
                {this.props.apis.map(api => <SideListItemContainer handleEdit={this.onEdit} key={api._id} name={api.name} id={api._id} resources={api.resources} />  )}
                {this.state.loading && <Spinner/>}
                {this.props.apis.length <= 0 ? ApiSideContainer.renderNoApiMessage(): null}
            </React.Fragment>
        );
    }
}

ApiSideContainer.propTypes = {
    apis: PropTypes.arrayOf(PropTypes.object).isRequired,
    fetch: PropTypes.func.isRequired,
    fetchOne: PropTypes.func.isRequired,
    add: PropTypes.func.isRequired,
    update: PropTypes.func.isRequired,
    alert: PropTypes.shape({
        error: PropTypes.func.isRequired
    }).isRequired
}

const mapStateToProps = state => {
    return {
        apis: state.api.list
    }
}

const container = compose(
    connect(mapStateToProps, { fetch, add, fetchOne, update }),
    withAlert()
)

export default container(ApiSideContainer);