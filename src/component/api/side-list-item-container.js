import ListItem from '@material-ui/core/ListItem';
import React from 'react';
import { PropTypes } from "prop-types";
import ExpandLess from '@material-ui/icons/ExpandLess';
import List from '@material-ui/core/List';
import { connect } from "react-redux";
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import uuid from "uuid";
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import SideListItem from './side-list-item';
import More from './more';
import { remove } from "./../../action/api";
import Resource from "../resource/side-item";

class SideListItemContainer extends React.Component {
    
    state = { 
        open: false
    }

    handleDelete = id => {
        this.props.remove(id);
    }
    
    toggleOpen = () => {
        this.setState({open: !this.state.open});
    }

    render() { 
        return (
            <React.Fragment>
                <ListItem >
                    <SideListItem name={this.props.name}/>
                    <More handleEdit={() => this.props.handleEdit(this.props.id)} onDelete={this.handleDelete} id={this.props.id} />
                    {this.state.open ? <ExpandLess style={{ cursor: "pointer" }} onClick={this.toggleOpen} /> : <ExpandMore style={{ cursor: "pointer" }} onClick={this.toggleOpen} />}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        {this.props.resources && this.props.resources.map(resource => <Resource key={uuid.v1()} name={resource.name} />)}
                    </List>
                    {!this.props.resources || this.props.resources.length <= 0 ? <List component="div" disablePadding>
                        <ListItem>
                            <ListItemText secondary="No Resources created yet"/>
                        </ListItem>
                    </List>: null}
                    <Divider/>
                </Collapse>
            </React.Fragment> 
        );
    }
}

SideListItemContainer.propTypes = {
    name: PropTypes.string.isRequired,
    remove: PropTypes.func.isRequired,
    handleEdit: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
    resources: PropTypes.shape({
        length: PropTypes.number,
        map: PropTypes.func
    })
}

SideListItemContainer.defaultProps = {
    resources: null
}

export default connect(null, { remove })(SideListItemContainer);