import React, { useState } from 'react';
import { InputGroup, Button, Form, Spinner } from "react-bootstrap";
import { PropTypes } from "prop-types";

const SideFrom = (props) => {

    const { handleChange, handleSubmit, name, btnSaveName, loading } = props;
    const [validated, setValidated] = useState(false);

    const onSubmit = e => {
        e.preventDefault();
        e.stopPropagation();
        const form = e.currentTarget;
        if (form.checkValidity() !== false) {
            return handleSubmit(e)
        }

        return setValidated(true);
    };
    
    return (
        <Form noValidate style={{ display: 'block', width: '100%' }} validated={validated} onSubmit={onSubmit} >
            <InputGroup className="mb-3">
                <Form.Control
                    type='text'
                    placeholder="Add new API"
                    aria-label="Add new API"
                    aria-describedby="basic-addon2"
                    onChange={handleChange}
                    required
                    value={name}
                    />
                <InputGroup.Append>
                    <Button type='submit' variant="outline-success" >
                        { loading && 
                            <Spinner
                                as="span"
                                animation="border"
                                size="sm"
                                role="status"
                                aria-hidden="true"
                            />
                        }
                        { !loading && btnSaveName}
                    </Button>
                </InputGroup.Append>
                <Form.Control.Feedback type="invalid">
                    Please choose descriptive name.
                </Form.Control.Feedback>
            </InputGroup>
        </Form>
    );
}

SideFrom.propTypes = {
    handleChange: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    name: PropTypes.string,
    btnSaveName: PropTypes.string.isRequired,
    loading: PropTypes.bool.isRequired
}

SideFrom.defaultProps = {
    name: ''
}

export default SideFrom;