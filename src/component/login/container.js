import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import LoginForm from './form';
import { login } from './../../action/auth';

class LoginContainer extends React.Component {
    state = {
        formData: {
            email: '',
            password: ''
        },
        loading: false,
        error: null
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({loading: true, error: null})
        this.props.login(this.state.formData)
            .then( () => this.setState({loading: false}))
            .catch( error => this.setState({loading: false, error: error.response.data}));
    }

    handleChange = (e) => {
        const {formData} = this.state;
        formData[e.target.name]= e.target.value
        this.setState({ formData, error: null });
    }

    render() { 
        return ( 
            <LoginForm onChange={this.handleChange} onSubmit={this.handleSubmit} data={this.state.formData} loading={this.state.loading} error={this.state.error} />
        );
    }
}

LoginContainer.propTypes = {
    login: PropTypes.func.isRequired
}

export default connect(null, {login})(LoginContainer);