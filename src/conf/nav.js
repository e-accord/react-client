import React from 'react';
import Home from './../component/home/home'
import ResourceFormContainer from './../component/resource/main-form-container'
import TeamFormContainer from './../component/team/main-form-container';

// issue with lazy https://github.com/ReactTraining/react-router/issues/6420?source=post_page---------------------------
// keep using simple import until the issue fix done

const navs = [
    {
        path: '/',
        exact: true,
        component: Home
    },
    {
        path: '/apis/:id/add-resource',
        exact: true,
        component: ResourceFormContainer
    },
    {
        path: '/apis/:id/add-description',
        exact: true,
        component: () => <h1>Edit Api</h1>
    },
    {
        path: '/teams/add-team',
        exact: true,
        component: TeamFormContainer
    }
]

export default navs;