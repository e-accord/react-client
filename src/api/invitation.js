import axios from 'axios';

export default {
    add: (guests, team) => axios.post('/users/invitation', {guests, teamId: team}).then(res => res.data)
}