import axios from 'axios';

export default {
    login: payload => axios.post('/users/auth', {...payload}).then(res => res.data),
    check: () => axios.get('/users/auth/check')
}