import {userLogedOut} from './../../action/auth'

const getErrorMessage = (status) => {
    let errorMessage = 'Something went wrong'

    switch (status) {
        case 500:
            return 'Something went wrong with the server';
        case 401:
        case 403:
            return 'Session invalid, please login';
        default:
            return errorMessage;
    }
}

export default (error, dispatch) => {

    const message = getErrorMessage(error.response.status)
    
    if (error.response.status === 403 || error.response.status === 401){
        dispatch(userLogedOut());
    }

    return Promise.reject(message);    
}