import axios from "axios";
import errorHandler from './error';

export default dispatch => {
    axios.interceptors.response.use(response => response, error => errorHandler(error, dispatch));
}