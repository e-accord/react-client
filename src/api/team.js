import axios from 'axios';

export default {
    add: name => axios.post('/users/team/', name).then(res => res.data)
}