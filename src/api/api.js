import axios from 'axios';
import intercept from './handler/interceptor';

const actions = {
    list: () => axios.get("/apis/").then(res => res.data),
    add: (name, description) => axios.post('/apis/', {name, description}).then(res => res.data),
    remove: id => axios.delete('/apis/', {_id: id}).then(res => res.data),
    get: id => axios.get(`/apis/${id}`).then(res => res.data),
    update: payload => axios.put('/apis', {...payload}).then(res => res.data)
}

const endpoints = dispatch => {
    intercept(dispatch);
    return actions;
}

export default endpoints;